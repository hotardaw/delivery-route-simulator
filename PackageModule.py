"""
This class defines a `Package` object that represents the delivery packages with various attributes and behaviors from the project rubric.

Classes:
    Package: A class representing a parcel with key ID and values of essential information such as address, weight, status, and delivery timing.


Notes:
    - The `update_delivery_status` method adjusts the status of the package based on its departure and delivery times.
    - The departure and delivery times can be set using the `departure_time` and `delivery_time` attributes.
    - The provided `convert_timedelta` parameter should be a `datetime.timedelta` object used for comparison."""


class Package:
    def __init__(self, ID, address, city, state, zipcode, deadline_time, weight, status):
        self.id = ID
        self.address = address
        self.city = city
        self.state = state
        self.zipcode = zipcode
        self.deadline_time = deadline_time
        self.weight = weight
        self.status = status
        self.departure_time = None
        self.delivery_time = None

    def __str__(self):
        return f"{self.id}, {self.address}, {self.city}, {self.state}, {self.zipcode}, {self.deadline_time}, {self.weight}, {self.delivery_time}, {self.status}"

    def update_delivery_status(self, hh_mm_time):
        if self.delivery_time and self.departure_time:
            if hh_mm_time >= self.delivery_time:
                self.status = "Delivered"
            elif self.departure_time <= hh_mm_time < self.delivery_time:
                self.status = "En route"
            else:
                self.status = "At hub"
        elif self.departure_time and not self.delivery_time:
            if hh_mm_time >= self.departure_time:
                self.status = "En route"
            else:
                self.status = "At hub"
        else:
            self.status = "At hub"

        # Replaced old logic:

        # if self.delivery_time < hh_mm_time:
        #     self.status = "Delivered"
        # elif self.departure_time > hh_mm_time:
        #     self.status = "En route"
        # else:
        #     self.status = "At hub"
